defmodule IdenticonTest do
  use ExUnit.Case
  doctest Identicon

  test "create_hash should create the hex value for given input" do
    %Identicon.ImageDto{hex: hex} = Identicon.create_hash("kunal")

    assert hex == [179, 58, 70, 245, 238, 129, 246, 240, 121, 15, 62, 169, 240, 36, 104, 225]
  end

  test "pick_color should pick the color from hex value" do
    hex = [179, 58, 70, 245, 238, 129, 246, 240, 121, 15, 62, 169, 240, 36, 104, 225]
    %Identicon.ImageDto{color: color} = Identicon.pick_color(Identicon.ImageDto.new(hex))

    assert color = [179, 58, 70]
  end
end
