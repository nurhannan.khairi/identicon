defmodule Identicon.ImageDto do
  defstruct hex: nil, color: nil, grid: nil, pixel_map: nil

  def new(hex) do
    %Identicon.ImageDto{hex: hex}
  end
end
