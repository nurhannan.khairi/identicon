defmodule Identicon do
  @moduledoc """
  Documentation for Identicon.
  """

  def main(input) do
    input
    |> create_hash
    |> pick_color
    |> create_grid
    |> filter_odd_squares
    |> create_pixel_map
    |> draw_identicon
    |> save_image_on_disc(input)
  end

  @doc """
    This function will create the hash from given string input using `:crypto` module.
    This function will return a `ImageDto` struct with hash value in the form of tuple in `hex`.

    Examples

        iex> Identicon.create_hash("kunal")
        %Identicon.ImageDto{
          color: nil,
          hex: [179, 58, 70, 245, 238, 129, 246, 240, 121, 15, 62, 169, 240, 36, 104,
           225]
        }

  """
  def create_hash(input) do
    hex =
      :crypto.hash(:md5, input)
      |> :binary.bin_to_list

      Identicon.ImageDto.new(hex)
  end

  @doc """
    This function will pick the first three elements in the `hex` tuple to form the Red, Green and Blue values for the color of identicon.
  """
  def pick_color(%Identicon.ImageDto{hex: [r, g, b | _tail]} = image_dto) do

    %Identicon.ImageDto{image_dto | color: {r, g, b}}
  end

  def create_grid(%Identicon.ImageDto{hex: hex} = image_dto) do
    grid =
      hex
      |> Enum.chunk_every(3, 3, :discard)
      |> Enum.map(&create_row/1)
      |> List.flatten
      |> Enum.with_index

    %Identicon.ImageDto{image_dto | grid: grid}
  end

  def create_row(row) do
    [first, second | _tail] = row
    row ++ [second, first]
  end

  def filter_odd_squares(%Identicon.ImageDto{grid: grid} = image_dto) do

    %Identicon.ImageDto{image_dto | grid: Enum.filter(grid, fn {code, _index} -> rem(code, 2) == 0 end)}
  end

  def create_pixel_map(%Identicon.ImageDto{grid: grid} = image_dto) do
    pixel_map = Enum.map(grid, fn {_code, index} -> create_x_y(index) end)

    %Identicon.ImageDto{image_dto | pixel_map: pixel_map}
  end

  def create_x_y(index) do
    x = rem(index, 5) * 50
    y = div(index, 5) * 50

    {{x, y}, {x + 50, y + 50}}
  end

  def draw_identicon(%Identicon.ImageDto{color: color, pixel_map: pixel_map}) do
    image = :egd.create(250, 250)
    fill = :egd.color(color)

    Enum.each(pixel_map, fn ({top_left, bottom_right}) ->
      :egd.filledRectangle(image, top_left, bottom_right, fill)
    end)

    :egd.render(image)
  end

  def save_image_on_disc(image, file_name) do
    File.write("#{file_name}.png", image)
  end
end
